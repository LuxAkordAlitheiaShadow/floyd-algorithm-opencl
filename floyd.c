//
// Created by Titouan 'LuX' Allain on 12/6/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <OpenCL/opencl.h> //TODO be careful is the APPLE path !


const char* program_source =
        "__kernel\n"
        "void exec_floyd(__global int* graph, int size_of_graph, int k_index)\n"
        "{\n"
        "   int i_index, j_index;\n"
        "   i_index = get_global_id(0);\n"
        "   j_index = get_global_id(1);\n"
        "   if ( graph[i_index*size_of_graph+k_index] + graph[k_index*size_of_graph + j_index] < graph[i_index*size_of_graph + j_index] )\n"
        "   {\n"
        "       graph[i_index*size_of_graph +j_index] = graph[i_index*size_of_graph + k_index] + graph[k_index*size_of_graph + j_index];\n"
        "   }\n"
        "}\n"
        ;



int main(int argc, char** arcv)
{
    // Bad number of argument statement
    if (argc != 2)
    {
        printf("Error : Bad number of argument !\nPleaser use `./floyd [n]`.\n");
    }
    // Right number of argument statement
    else
    {
        int size_of_graph = atoi(arcv[1]);

        // Declaring graphs
        int* one_line_graph;
        int* one_line_result;
//TODO secure malloc
        one_line_graph = malloc(size_of_graph * size_of_graph * sizeof(int));
        one_line_result = malloc(size_of_graph * size_of_graph * sizeof(int));

        // Initializing graph
        int line;
        int elem_in_line;
        for ( line = 0 ; line < size_of_graph ; line ++ )
        {
            for ( elem_in_line = 0 ; elem_in_line < size_of_graph ; elem_in_line++ )
            {
                // Relation of the same node statement
                if ( elem_in_line == line )
                {
                    one_line_graph[line * size_of_graph + elem_in_line] = 0;
                }
                // Relation between next and previous node statement
                else if ( elem_in_line == ((line + 1)%size_of_graph) )
                {
                    one_line_graph[line * size_of_graph + elem_in_line] = 1;
                }
                // Relation to another node statement
                else
                {
                    one_line_graph[line * size_of_graph + elem_in_line] = size_of_graph + 1;
                }
                // Printing value
//                printf("%d ",one_line_graph[line * size_of_graph + elem_in_line]);
            }
//            printf("\n");
        }
//        printf("\n");

        // Initializing  CL variable
        cl_int status;

        // Initializing CL platform variables
        cl_uint numPlatforms = 0;//TODO change name with _
        cl_platform_id *platforms = NULL;

        // Getting platform ID
        status = clGetPlatformIDs(0, NULL, &numPlatforms);
        platforms = (cl_platform_id*)malloc( numPlatforms*sizeof(cl_platform_id));
        status = clGetPlatformIDs(numPlatforms, platforms, NULL);

        // Getting platform information //TODO Useless but it's cool to show what is your platform
        char platform_name[1000];
        clGetPlatformInfo(platforms[0], CL_PLATFORM_NAME, sizeof(platform_name), platform_name, NULL);
        printf("Name of platform : %s\n", platform_name);

        // Init CL devices variables
        cl_uint numDevices = 0;//TODO change name with _
        cl_device_id *devices = NULL;

        // Getting device ID
        status = clGetDeviceIDs( platforms[0], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
        devices = (cl_device_id*)malloc(numDevices*sizeof(cl_device_id));
        status = clGetDeviceIDs( platforms[0], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);

        // Getting device information //TODO Useless but it's cool to show what is your devices
        char device_name[1000];
        // TODO for numDevices
        clGetDeviceInfo (devices[0], CL_DEVICE_NAME, sizeof(device_name), device_name, NULL);
        printf("Name of device 0: %s\n", device_name);
        clGetDeviceInfo (devices[1], CL_DEVICE_NAME, sizeof(device_name), device_name, NULL);
        printf("Name of device 1: %s\n", device_name);

        // Initializing CL context
        cl_context context = NULL;
        context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);

        if (status) printf("ERROR CONTEXT CREATION: %d\n", status);

        // Initializing CL command queue
        cl_command_queue cmd_queue;
        cmd_queue = clCreateCommandQueue(context, devices[1], 0, &status);

        // Initializing CL buffers variables
        cl_mem buffer_graph, buffer_result;
        size_t data_size = (size_of_graph * size_of_graph) * sizeof(int);
        buffer_graph = clCreateBuffer(context, CL_MEM_READ_WRITE, data_size, NULL, &status);

        // Writing graph to buffer
        status = clEnqueueWriteBuffer(cmd_queue, buffer_graph, CL_FALSE,0,data_size,one_line_graph,0,NULL,NULL);
        if (status) printf("ERREUR Buffer: %d\n", status);

        // Creating and compiling CL program
        cl_program program = clCreateProgramWithSource(context, 1, (const char**)&program_source, NULL, &status);
        status = clBuildProgram(program, numDevices, devices, NULL, NULL, NULL);
        if (status) printf("ERREUR A LA COMPILATION: %d\n", status);
        // TODO check status

        // Creating CL kernel
        cl_kernel kernel = clCreateKernel(program, "exec_floyd", &status);

        // Parsing arguments to kernel
        status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &buffer_graph);
        status = clSetKernelArg(kernel, 1, sizeof(int), (void *)&size_of_graph);

        // Initializing CL WorkSize
        size_t global_work_size[2];
        global_work_size[0] = size_of_graph;
        global_work_size[1] = size_of_graph;

        // Enqueuing kernel
        int k_index;
        for ( k_index = 0 ; k_index < size_of_graph ; k_index++ )
        {
            status = clSetKernelArg(kernel, 2, sizeof(int), (void *)&k_index);
            status = clEnqueueNDRangeKernel(cmd_queue, kernel, 2, NULL, global_work_size, NULL, 0, NULL, NULL);
        }

        // Reading result
        clEnqueueReadBuffer(cmd_queue, buffer_graph, CL_TRUE, 0, data_size, one_line_result, 0, NULL, NULL);

        // Printing result
        for (line = 0 ; line < size_of_graph ; line++)
        {
            for (elem_in_line = 0 ; elem_in_line < size_of_graph ; elem_in_line++)
            {
                printf("%d ", one_line_result[line * size_of_graph + elem_in_line]);
            }
            printf("\n\n");
        }

        // Releasing CL resources
        clReleaseKernel(kernel);
        clReleaseProgram(program);
        clReleaseCommandQueue(cmd_queue);
        clReleaseMemObject(buffer_graph);
        clReleaseMemObject(buffer_result);
        clReleaseContext(context);
        free(platforms);
        free(devices);

        // Releasing array
        free(one_line_graph);
        free(one_line_result);
    }
    return 0; // Success exit
}
